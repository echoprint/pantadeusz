package pl.sda.panTadeusz;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

class Rhymes {

    static void showNumberOfAllRhymesWhichAppeardMoreThanOnce(Map<String, Integer> countedRhymes) {
        long countR = countedRhymes.entrySet().stream()
                .filter(m -> m.getValue() > 1)
                .count();
        System.out.println("Number of rhymes which appeard more than once: " + countR);
    }

    static Map<String, Integer> showRhymes(List<String> rhymesPair) {
        Map<String, Integer> countedRhymes = countRhymes(rhymesPair);
        System.out.println("List of rhymes which appeard more than once");
        countedRhymes.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .filter(m -> m.getValue() > 1)
                .forEach(System.out::println);
        return countedRhymes;
    }

    static Map<String, Integer> countRhymes(List<String> rhymesPair) {
        int counter;
        Map<String, Integer> countedRhymes = new HashMap<>();
        for (String r : rhymesPair) {
            counter = countedRhymes.getOrDefault(r, 0);
            countedRhymes.put(r, counter + 1);
        }
        System.out.println("Number of all rhymes in text: " + countedRhymes.size());
        return countedRhymes;
    }

    static List<String> reduceRhymes(List<String> lastWordsList) {
        List<String> rhymesPair = new ArrayList<>();
        Iterator<String> iter = lastWordsList.iterator();
        while (iter.hasNext()) {

            String s1 = iter.next();
            String s2 = iter.next();
            if (s1.equals(s2)) {
                rhymesPair.add(s1);
            }
        }
        return rhymesPair;
    }

    static List<String> getLast3CharactersLastWordOfVerse() throws FileNotFoundException {
        File file = new File("src/main/resources/pan-tadeusz.txt");
        Scanner inLine = new Scanner(file);
        String lastWord;
        int lineCounter = 0;
        List<String> lastWordsList = new ArrayList<>();
        String verse;
        String rhyme;

        while (inLine.hasNextLine()) {
            verse = inLine.nextLine().toLowerCase();

            if (verse.length() == 0) {
                continue;
            }
            lineCounter++;

            String[] parts = verse.split(" ");

            lastWord = parts[parts.length - 1];
            if (lastWord.length() == 1) {
                lastWord = parts[parts.length - 2];
            }

            lastWord = lastWord.replaceAll("[.,():–*;?!…»«]", "");

            if (lineCounter <= 6 || lineCounter > 9948) {
                continue;
            }

            if (lastWord.length() > 3) {
                rhyme = lastWord.substring(lastWord.length() - 3);
                lastWordsList.add(rhyme);
            }


        }
        System.out.println("Number of last 3 characters of the verse: " + lastWordsList.size());
        return lastWordsList;

    }


}
