package pl.sda.panTadeusz;

import java.io.FileNotFoundException;
import java.util.*;


public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        Map<String, Integer> countedWords = Words.getCountedWordsMap();

        System.out.println("5 most common words in text: ");
        Words.mostPopularWords(countedWords);
        System.out.println();

        Words.showNumberOfWordsAppearingOnlyOnce(countedWords);

        Words.showListOfWordsAppearingOnlyOnce(countedWords);

        System.out.println();
        System.out.println("---RHYMES----");

        List<String> lastRhymesListing = Rhymes.getLast3CharactersLastWordOfVerse();
        System.out.println();

        List<String> rhymesPairs = Rhymes.reduceRhymes(lastRhymesListing);

        Map<String, Integer> countedRhymes = Rhymes.showRhymes(rhymesPairs);

        System.out.println();
        Rhymes.countRhymes(rhymesPairs);
        Rhymes.showNumberOfAllRhymesWhichAppeardMoreThanOnce(countedRhymes);

    }


}


