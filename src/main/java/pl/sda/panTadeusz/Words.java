package pl.sda.panTadeusz;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

class Words {
    static void showListOfWordsAppearingOnlyOnce(Map<String, Integer> countedWords) {
        List<Map.Entry<String, Integer>> collect = countedWords.entrySet().stream()
                .filter(m -> m.getValue() == 1)
                .collect(Collectors.toList());
        System.out.println(collect);
    }

    static void showNumberOfWordsAppearingOnlyOnce(Map<String, Integer> countedWords) {
        long unrepetedWords = countedWords.entrySet().stream()
                .filter(m -> m.getValue() == 1)
                .count();
        System.out.println("Number of words occuring only once: " + unrepetedWords);
    }

    static Map<String, Integer> getCountedWordsMap() throws FileNotFoundException {
        File file = new File("src/main/resources/pan-tadeusz.txt");
        Scanner in = new Scanner(file);
        List<String> words = new ArrayList<>();
        int count;
        String word;

        while (in.hasNext()) {

            word = in.next().toLowerCase();
            word = word.replaceAll("[.,():–*;?!…»«]", "");

            if (word.isEmpty()) {
                continue;
            }
            words.add(word);

        }
        System.out.println(words.size());

        Map<String, Integer> countedWords = new HashMap<>();
        for (String w : words) {
            count = countedWords.getOrDefault(w, 0);
            countedWords.put(w, count + 1);
        }
        return countedWords;
    }

    static void mostPopularWords(Map<String, Integer> countedWords) {
        countedWords.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .limit(5)
                .forEach(System.out::println);
    }

}
